<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \App\Middlewares\AccountValidation;

use App\Controllers\Chat;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

require './vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

require 'app/containers.php';

$app->get('/messages', \App\Controllers\PagesController::class . ':messages')->setName('Messages');
$app->get('/', \App\Controllers\PagesController::class . ':home')->setName('Home');
$app->get('/register', \App\Controllers\PagesController::class . ':register')->setName('Get_Register');
$app->post('/register', \App\Controllers\PagesController::class . ':register')
    ->setName('Post_Register')
    ->add(new AccountValidation());


$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->write("Hello, $name");

    return $response;
});

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Chat()
        )
    ),
    8080
);

$server->run();

$app->run();

?>