<?php

namespace App\Classes;

class Database {

    private static $_con = null;

    public function     __construct($DB_DSN, $DB_USER, $DB_PASSWORD) {
        if (isset(self::$_con))
                return (true);
        try  {
            self::$_con = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
            self::$_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$_con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            return (true);
        } catch (PDOException $e) {
            if (isset($e))
                return (false);
        }
    }

    public function      lastId() {
        $stm = self::$_con->query("SELECT LAST_INSERT_ID()");
        return ($stm->fetchColumn());
    }

    public function     init() {
        
    }

    public function     nonQuery($query, $params = []) {
        $stm = self::$_con->prepare($query);
        $result = $stm->execute($params);
        $stm->closeCursor();
        if ($result === false)
            return (false);
        return (true);
    }

    public function     readQuery($query, $params = [], $options = []) {
        $stm = self::$_con->prepare($query);
        $error = $stm->execute($params) === false;
        if (count($options) === 0)
        {
            $options = [
                'mode' => PDO::ATTR_DEFAULT_FETCH_MODE,
                'class' => null
            ];
        }
        if ($error === false)
            $result = $stm->fetchAll($options['mode'], $options['class']);
        $stm->closeCursor();
        if ($error === false)
            return ($result);
        return (false);
    }

    public function     readRowQuery($query, $params = []) {
        $stm = self::$_con->prepare($query);
        $error = $stm->execute($params) === false;
        if ($error === false)
            $result = $stm->fetchColumn();
        $stm->closeCursor();
        if ($error === false)
            return ($result);
        return (false);
    }

}