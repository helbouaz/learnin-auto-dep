<?php

namespace App\Middlewares;

use Interop\Container\ContainerInterface;

class AccountValidation {

    public function     __construct() {
        
    }

    private function    _validPass($pass) {
        if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/", $pass) != 1)
            return (false);
        return (true);
    }

    private function    _validUsername($username) {
        if (strlen($username) <= 4 || preg_match("/^\d*[a-zA-Z][a-zA-Z\d]*$/", $username) != 1)
            return (false);
        return (true);
    }

    private function    _validEmail($eMail) {
        if (!filter_var($_POST['newmail'], FILTER_VALIDATE_EMAIL))
            return (false);
        return (true);
    }

    public function     __invoke($request, $response, $next) {
        $params = $request->getParams();
        $errors = [];

        if (empty($params['username']))
            $errors['username'] = 'Please enter a username.';
        else if (!$this->_validUsername($params['username']))
            $errors['username'] = 'Please enter a valid username.';

        if (empty($params['email']))
            $errors['email'] = 'Please enter an E-mail.';
        else if (!$this->_validEmail($params['email']))
            $errors['email'] = 'Please enter a valid E-mail.';

        if (empty($params['password']) || empty($params['repassword']))
            $errors['password'] = 'Please enter a password.';
        else if ($params['password'] != $params['repassword'])
            $errors['password'] = 'Passwords do not match.';
        else if (!$this->_validPass($params['password']))
            $errors['password'] = 'Password not strong enough.';

        $request = $request->withAttributes($errors);

        return ($next($request, $response));
    }

}