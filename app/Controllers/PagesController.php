<?php

namespace App\Controllers;

class PagesController extends BaseController {

    public function     home($request, $response) {
        $this->render($response, 'home.twig', [
            'errors' => [
                'mail' => "Error in mail.",
                'username' => "Error in username"
            ]
        ]);
    }

    public function     register($request, $response) {
        $errors = $request->getAttributes();
        // var_dump($request->getAttributes());
        // var_dump($errors);
        $this->render($response, 'auth/register.twig', $errors);
    }

    public function     messages($request, $response) {
        $this->render($response, 'messages/main.twig');
    }

}