<?php

namespace App\Controllers;

abstract class BaseController {

    protected   $container;

    public function     __construct($container) {
        $this->container = $container;
    }

    public function     render($response, $file, $params = []) {
        $this->container->view->render($response, $file, $params);
    }

}